function distanceMatrix = generateRandomDistanceMatrix(coordMatrix,max)
    % generateRandomDistanceMatrix
    % description: Generates an asymmetrical distance matrix based on a
    % coordinate matrix.
    % date: 8 Oct 2008
    
    n = size(coordMatrix,1);
    distanceMatrix = zeros(n,n);
    
    for i=1:n
        for j=1:n
            d = unidrnd(max);
            
            if j == i
                distanceMatrix(i,j) = 0;
            else
                distanceMatrix(i,j) = d;
            end
        end
    end
end