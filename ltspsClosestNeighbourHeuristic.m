function [route, length] = ltspsClosestNeighbourHeuristic (distanceMatrix)
    % Trivial wrapper around the ltspsGreedyHeuristic function to provide a
    % heuristic for the closest neighbour without having to add a
    % parameter.
    [route, length] = ltspsGreedyHeuristic(distanceMatrix,1);
end