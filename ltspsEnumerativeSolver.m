function [bestRoute, bestLength] = ltspsEnumerativeSolver (distanceMatrix)
matrixDims = size(distanceMatrix);
numCities = matrixDims(1,1);

% Initialisations
% Shortest route until now and its length
bestRoute = zeros(1,numCities+1);
bestLength = realmax; 

permutations = perms(1:numCities);

% This matrix has:
%  * n! rows (the permutations, minus the one for the first vertex)
%  * n  cols (the number of cities, minus the one for the first vertex)

rowLimit = factorial(numCities);
colLimit = numCities;

for i = 1:rowLimit
    % Get the permutation (= the route minus the first and last vertex)
    thisRoute = permutations(i,1:colLimit);
    
    currentCity     = thisRoute(1);
    currentDistance = 0;
    
    % While there are still cities to travel to...
    for j = 1:colLimit
        % Find the new distance and travel it.
        newDistance = distanceMatrix(currentCity,thisRoute(1,j));
        currentDistance = currentDistance + newDistance;
        
        % Place yourself in the next city.
        currentCity = thisRoute(1,j);
    end
        
    if currentDistance < bestLength
        bestLength = currentDistance;
        bestRoute = [thisRoute thisRoute(1)];
    end
end

end