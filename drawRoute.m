function drawRoute (route, coordsMatrix)
    % drawRoute
    % description: draws a given TSP route on the figure.
    % author: Laurens Van Houtven <lvh@laurensvh.be>
    % date: 4 Oct 2008
    
    % Find the length of the route.
    routeDims = size(route);
    routeLen = routeDims(2);
    
    % Extract coordinate vectors for easy access.
    xSourceCoords = coordsMatrix(:,2);
    ySourceCoords = coordsMatrix(:,3);
    
    % Initialize drawing vectors for speed.
    xDrawCoords = zeros(routeLen);
    yDrawCoords = zeros(routeLen);
    
    for i = 1:routeLen
        % Where are we?
        thisVertex = route(i);
        
        % Add the coordinates of this vertex to the draw vector.
        xDrawCoords(i) = xSourceCoords(thisVertex);
        yDrawCoords(i) = ySourceCoords(thisVertex);
    end
    
    % Plot the two drawvectors with a yellow line between them.
    plot(xDrawCoords, yDrawCoords, 'Color', 'blue');
    
    % Draw it on the current frame.
    hold on;
    drawnow;
end