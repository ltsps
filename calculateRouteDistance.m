function totalDistance = calculateRouteDistance (route, distanceMatrix)
% calculateRouteDistance
% description: Calculates the distance travelled while following a route,
% given the (possibly symmetric) distance matrix (rows are sources, columns
% are destinations) and the route that is followed. Assumes routes are
% cyclic, but works fine if they're not (besides complaining a little).
% author: Laurens Van Houtven <lvh@laurensvh.be>
% date: 2 Oct 2008

% Do a few rudimentary checks on input. Don't expect intelligent error
% messages to be returned. Set this to any true value to do the checks, any
% false value to disable them.
DO_INPUT_CHECKS = 1;

% What are the dimensions of the distance matrix? (or: # of cities?)
matrixDims = size(distanceMatrix);
% How many cities are there?
nCities = matrixDims(2);

% What are the dimensions of the route vector? (should be n+1)
routeDims = size(route);
% How many cities are there in the route?
routeSize = routeDims(2);

if DO_INPUT_CHECKS
    % TODO: sprintf for better error messages.
    % Apparently this is not part of the assignment anyway, so not wasting
    % time on this right now.
    
    if matrixDims(1) ~= matrixDims(2)
        % Danger, Will Robinson, danger! Distance matrix not square.
        disp('The distance matrix you gave me is not square.')
    end

    if  routeSize ~= (nCities+1)
        % Route either has too many or too few entries!
        disp('The route doesnt have the number of cities I was expecting.')
    end

    if route(1) ~= route(routeSize)
        % Route is not cyclic!
        disp('The last node in the given route is not the same as the first.')
    end
end % input checks

totalDistance = 0;

for i = 1:(routeSize-1)
    % routeSize-1 and nCities are equivalent and nCities is arguably
    % slightly more elegant, but routeSize-1 makes more sense in this
    % algorithm because we are adding the distance from nodes i to i+1
    % every time we evaluate this loop body.
    
    city1 = route(i);
    city2 = route(i+1);

    % Calculate the distance between i and i+1
    newDistance   = distanceMatrix(city1, city2);
    totalDistance = totalDistance + newDistance; % matlab has no +=??? 
end

end
