function locationMatrix = importLocation

[name,path]=uigetfile('*.txt');
dataPath = [path,name];
locationMatrix = importdata(dataPath);