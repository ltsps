function [route, length] = ltspsFurthestNeighbourHeuristic (distanceMatrix)
    % Trivial wrapper around the ltspsGreedyHeuristic function to provide a
    % heuristic for the furthest neighbour without having to add a
    % parameter.
    [route, length] = ltspsGreedyHeuristic(distanceMatrix,0);
end