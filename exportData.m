function exportData (randomR,randomD,nearestR,nearestD,farthestR,...
    farthestD,enumR,enumD,coordMatrix,distanceMatrix)
% Prompts the user to specify a location to save the file containing the
% exported TSP data and then exports it to the specified file.
% author: Gert Thijs
% date: 9 Oct 2008

% Specify filepath
[FileName,PathName] = uiputfile('*.txt','Export Data','ExportData');
filePath = [PathName FileName];

% Open the file in write mode
fid = fopen(filePath, 'wt');

% Create the header for the file
fprintf(fid,'### TRAVELLING SALESMAN PROBLEM ###\n\n');

% Enter the matrix data
fprintf(fid,'=== Location Matrix ===\n\n');
fprintf(fid,'-#-\t -X-\t -Y-\t\n\n');
fprintf(fid,'%.0f\t %.0f\t %.0f\t\n',coordMatrix');
fprintf(fid,'\n\n');

fprintf(fid,'=== Distance Matrix ===\n\n');
for i = 1:size(distanceMatrix,1)
    for j = 1:size(distanceMatrix,2)
        fprintf(fid,'%.0f\t',distanceMatrix(i,j));
    end
    fprintf(fid,'\n');
end
fprintf(fid,'\n\n');

% Enter the Random Heuristic data
fprintf(fid,'=== Random Heuristic ===\n\n');
fprintf(fid,'Route:\t\t');
fprintf(fid,randomR);
fprintf(fid,'\n\n');
fprintf(fid,'Distance:\t');
fprintf(fid,'%.3f km\n\n',randomD);

% Enter the Nearest Neighbour Heuristic data
fprintf(fid,'=== Nearest Neighbour Heuristic ===\n\n');
fprintf(fid,'Route:\t\t');
fprintf(fid,nearestR);
fprintf(fid,'\n\n');
fprintf(fid,'Distance:\t');
fprintf(fid,'%.3f km\n\n',nearestD);

% Enter the Farthest Neighbour Heuristic data
fprintf(fid,'=== Farthest Neighbour Heuristic ===\n\n');
fprintf(fid,'Route:\t\t');
fprintf(fid,farthestR);
fprintf(fid,'\n\n');
fprintf(fid,'Distance:\t');
fprintf(fid,'%.3f km\n\n',farthestD);

% Enter the Enumerative Heuristic data
fprintf(fid,'=== Enumerative Heuristic ===\n\n');
if enumD ~= 0
    fprintf(fid,'Route:\t\t');
    fprintf(fid,enumR);
    fprintf(fid,'\n\n');
    fprintf(fid,'Distance:\t');
    fprintf(fid,'%.3f km\n',enumD);
else
    fprintf(fid,'Route:\t\t');
    fprintf(fid,'Not Available\n\n');
    fprintf(fid,'Distance:\t');
    fprintf(fid,'Not Available\n');
end
fclose(fid);