function drawCities (coordsMatrix)
    % drawCities
    % description: Draws a dot on the map for each city.
    % author: Laurens Van Houtven <lvh@laurensvh.be>
    % date: 3 Oct 2008
    
    % Extract coordinate vectors for easy access.
    xCoords = coordsMatrix(:,2);
    yCoords = coordsMatrix(:,3);
    
    cla;    
    xAxisMin = 0; yAxisMin = 0;
    [xAxisMax yAxisMax] = getAxisMaximums(xCoords, yCoords);
    
    plot(xCoords, yCoords, 'or'); % draw red circles
    axis([xAxisMin xAxisMax yAxisMin yAxisMax]);
    
    
    hold on;
    drawnow;
end

function [xAxisMax, yAxisMax] = getAxisMaximums (xCoords, yCoords)
    % getAxisMaximums
    % description: Tries to find decent axis maximums for the given data.
    % author: Laurens Van Houtven <lvh@laurensvh.be>
    % date: 3 Oct 2008
    
    % Verbosity for debugging purposes.
    VERBOSE = 0;
    
    if max(xCoords) < 100
        xAxisMax = (ceil(max(xCoords)/10)+1)*10;
    else
        xAxisMax = (ceil(max(xCoords)/100)+1)*100;
    end
    
    if max(yCoords) < 100
        yAxisMax = (ceil(max(yCoords)/10)+1)*10;
    else
        yAxisMax = (ceil(max(yCoords)/100)+1)*100;
    end         
    
    if VERBOSE
        display(xAxisMax);
        display(yAxisMax);
    end
end
