function distanceMatrix = importDistance

[name,path]=uigetfile('*.txt');
dataPath = [path,name];
distanceMatrix = importdata(dataPath);