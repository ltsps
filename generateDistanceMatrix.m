function distanceMatrix = generateDistanceMatrix (coordMatrix)
% generateDistanceMatrix
% description: Generates a distance matrix from a matrix of coordinates.
% author: Laurens Van Houtven <lvh@laurensvh.be>
% date: 30 Sep 2008

% Sort the matrix by first row (key)
sorted = sortrows(coordMatrix,1);

% How many nodes?
matrixSize = size(sorted);
nCities = matrixSize(1);

% Preallocation to prevent in-loop growing
distanceMatrix = zeros(nCities,nCities);

for i = 1:nCities
	% coords of first city
	x1 = sorted(i,2);
	y1 = sorted(i,3);
	% location vector of the first city
	c1 = [x1 y1];
    
    for j = i:nCities
        % 1:nCities would be stupid -- we'd recalculate the 1->i stuff
        % which we already calculated in the previous loop.
        
        % Where is the second city?
	    % coords of the second city
	    x2 = sorted(j,2);
	    y2 = sorted(j,3);
	    % location vector of the second city
	    c2 = [x2 y2];
        
        % Where is the second city relative to the first?
	    % vector from c1 to c2
	    c12vec = c2 - c1;
	    % How farm is the second city from the first?
        % distance from c1 to c2 is the norm of the vector between them
	    c12dist = norm(c12vec);
	    
        % Save those distances.
        % add the distance to the matrix
	    distanceMatrix(i,j) = c12dist;
	    % when in doubt, problem is symmetric
	    distanceMatrix(j,i) = c12dist;
    end % inner for, distances for city j are done
end % outer for, distances for city i are done

end % function