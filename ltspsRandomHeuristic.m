function [route, distance] = ltspsRandomHeuristic (distanceMatrix)
% ltspsRandomHeuristic
% description: a heuristic tsp solver that picks random vertices
% author: Laurens Van Houtven <lvh@laurensvh.be>
% date: 28 Sep 2008

% What are the dimensions of the distance matrix? (or: # of cities?)
matrixDims = size(distanceMatrix);
% How many cities are there?
nCities = matrixDims(2);
% Preallocate the route.
route = zeros(1,nCities+1);
% Potential city candidates.
source = 1:nCities;

for i = 1:nCities
    % How many cities do we still get to pick from?
    sourceSize = size(source);
    citiesLeft = sourceSize(2);
    
    % Pick a random index.
    thisIndex = unidrnd(citiesLeft);
    
    % Find the new city.
    newCity = source(thisIndex);
    
    % Add it to the route.
    route(i) = newCity;
    
    % Remove it from the pool of potential candidates.
    source = source(source ~= newCity);
end

% Make the route cyclic
route(end) = route(1);

distance = calculateRouteDistance(route, distanceMatrix);

end