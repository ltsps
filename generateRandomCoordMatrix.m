function coordMatrix = generateRandomCoordMatrix(n, max)
    % generateRandomCoordMatrix
    % description: Generates a random coordinate matrix.
    % date: 8 Oct 2008
    coordMatrix = zeros(n,3);
    
    for i=1:n
        x = unidrnd(max);
        y = unidrnd(max);
        
        coordMatrix(i,1) = i;
        coordMatrix(i,2) = x;
        coordMatrix(i,3) = y;
    end
end