use warnings;
use strict;

use Carp;
use Data::Dumper;
use Digest::MD5;

my $matlab    = "/usr/local/bin/matlab";
my $console   = " -nosplash -nodesktop -nojvm"; 
my $log       = " -logfile t/matlab_output/log";
my $stderrlog = "t/matlab_output/STDERR";
my $stdoutlog = "t/matlab_output/STDOUT";
my $d         = Digest::MD5->new;

sub gen_command {
    my $arg = shift;

    return $matlab.$console.$log." -r \"$arg; save 't/matlab_output/output' 'ans' -ascii;quit;\"";
}

sub run_matlab_cmd {
    my $cmd = gen_command(shift);

    local *STDERR;
    local *STDOUT;

    open(STDOUT, ">>", $stdoutlog) or die $!;
    open(STDERR, ">>", $stderrlog) or die $!;

    system($cmd);

    return 1;
}

sub scrub {
    unlink "t/matlab_output/output";
    unlink "t/matlab_output/log";
}

sub matlab_location {
    return $matlab;
}

sub compare {
    my ($expected_fn) = @_;

    open(my $expected, "<", $expected_fn);
    open(my $got,      "<", "t/matlab_output/output");

    $d->addfile($expected);
    my $d_expected = $d->hexdigest;

    $d->addfile($got);
    my $d_got = $d->hexdigest;

    if ($d_expected ne $d_got) {
        return;
    }
    else {
        return 1;
    }
}
