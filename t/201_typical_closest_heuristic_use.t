use warnings;
use strict;

use Test::More qw(no_plan);
use Digest::MD5;
use Data::Dumper;

require 't/lib/Common.pm';

my $erm;
my $tm;
my $d = Digest::MD5->new;

{
my $cmd = "M = [1 2 3; 2 3 2; 3 4 5; 4 5 4];";
$cmd   .= "D=generateDistanceMatrix(M); ";
$cmd   .= "ltspsClosestNeighbourHeuristic(M);";
run_matlab_cmd($cmd);
}

##
$erm = "output didn't match what I expected";
$tm  = "typical closest neighbour heuristic use (4 cities in a square)";

my $expected_fn = "t/expected_output/201_typical_closest_heuristic_use";

open(my $expected, "<", $expected_fn);
open(my $got,      "<", "t/matlab_output/output");

$d->addfile($expected);
my $d_expected = $d->hexdigest;

$d->addfile($got);
my $d_got = $d->hexdigest;

print Dumper($d_expected);
print Dumper($d_got);

ok(($d_expected eq $d_got or die($erm)),$tm);
##

