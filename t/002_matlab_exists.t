use warnings;
use strict;

use Test::More qw(no_plan);

require 't/lib/Common.pm';

my $m = matlab_location();

ok(-e $m or die("couldn't find matlab"), "matlab exists");
ok(-x $m or die("matlab isn't executable"), "matlab is executable");


