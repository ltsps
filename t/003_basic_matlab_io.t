use warnings;
use strict;

use Test::More qw(no_plan);
use Digest::MD5;
use Data::Dumper;

require 't/lib/Common.pm';

my $erm;
my $tm;
my $d = Digest::MD5->new;

{
my $cmd = "[1 1 1; 2 2 2; 3 3 3]";
run_matlab_cmd($cmd);
}

##
$erm = "output didn't match what I expected";
$tm  = "received the expected output";

my $expected_fn = "t/expected_output/003_basic_matlab_io";

open(my $expected, "<", $expected_fn);
open(my $got,      "<", "t/matlab_output/output");

$d->addfile($expected);
my $d_expected = $d->hexdigest;

$d->addfile($got);
my $d_got = $d->hexdigest;

print Dumper($d_expected);
print Dumper($d_got);

ok(($d_expected eq $d_got or die($erm)),$tm);
##
