use warnings;
use strict;

use Test::More qw(no_plan);

my @mfiles = qw(drawAll.m
                drawCities.m
                drawRoute.m
                calculateRouteDistance.m
                generateRandomCoordMatrix.m
                generateDistanceMatrix.m
                ltspsGreedyHeuristic.m
                ltspsClosestNeighbourHeuristic.m
                ltspsFurthestNeighbourHeuristic.m
                ltspsRandomHeuristic.m
                ltspsEnumerativeSolver.m
                );

foreach my $mfile (@mfiles) {
    ok(open(my $test, "<", $mfile)) or die("Can't open $mfile for reading");    
}
