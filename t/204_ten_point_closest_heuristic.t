use warnings;
use strict;

use Test::More qw(no_plan);
use Digest::MD5;
use Data::Dumper;

require 't/lib/Common.pm';

my $erm;
my $tm;
my $d = Digest::MD5->new;

{
my $cmd = "M = [1 7 7; 2 7 10; 3 3 8; 4 3 2; 5 7 5; 6 5 7; 7 8 4; 8 7 5; 9 9 9; 10 3 7];";
$cmd   .= "D = generateDistanceMatrix(M); ";
$cmd   .= "ltspsClosestNeighbourHeuristic(M);";
run_matlab_cmd($cmd);
}

##
$erm = "output didn't match what I expected";
$tm  = "ten point closest neighbour heuristic use";

my $expected_fn = "t/expected_output/204_ten_point_closest_heuristic";

open(my $expected, "<", $expected_fn);
open(my $got,      "<", "t/matlab_output/output");

$d->addfile($expected);
my $d_expected = $d->hexdigest;

$d->addfile($got);
my $d_got = $d->hexdigest;

print Dumper($d_expected);
print Dumper($d_got);

ok(($d_expected eq $d_got or die($erm)),$tm);
##

