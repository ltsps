use warnings;
use strict;

use Test::More qw(no_plan);
use Digest::MD5;
use Data::Dumper;

require 't/lib/Common.pm';

my $erm;
my $tm;
my $d = Digest::MD5->new;

{
my $cmd = "M = [1 0 0; 2 0 0; 3 0 0; 4 0 0];";
$cmd   .= "D=generateDistanceMatrix(M); ";
$cmd   .= "ltspsClosestNeighbourHeuristic(M);";
run_matlab_cmd($cmd);
}

##
$erm = "output didn't match what I expected";
$tm  = "pathological closest neighbour heuristic use (4 cities on the same point)";

my $expected_fn = "t/expected_output/301_pathological_closest_heuristic_use";

open(my $expected, "<", $expected_fn);
open(my $got,      "<", "t/matlab_output/output");

$d->addfile($expected);
my $d_expected = $d->hexdigest;

$d->addfile($got);
my $d_got = $d->hexdigest;

print Dumper($d_expected);
print Dumper($d_got);

ok(($d_expected eq $d_got or die($erm)),$tm);
##

