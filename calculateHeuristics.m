function [routeRandom, distanceRandom, routeNearest, distanceNearest, ...
    routeFarthest, distanceFarthest, routeEnum, distanceEnum] ...
    = calculateHeuristics (distanceMatrix)
% Calculate the best route and its distance using the different
% heuristics: Random, Nearest Neighbour, Farthest Neighbour and 
% Enumerative (if the number of cities does not exceed ten).

% Calculate the routes and distances using the three standard heuristics.
[routeRandom,distanceRandom] = ltspsRandomHeuristic(distanceMatrix);
[routeNearest,distanceNearest] = ltspsClosestNeighbourHeuristic(distanceMatrix);
[routeFarthest,distanceFarthest] = ltspsFurthestNeighbourHeuristic(distanceMatrix);

% Initialize the route and distance of the Enumerative solution.
routeEnum = 0;
distanceEnum = 0;

% Calculate using the Enumerative Solver if the number of cities does not
% exceed 10.
if size(distanceMatrix,1) <= 10
    [routeEnum,distanceEnum] = ltspsEnumerativeSolver(distanceMatrix);
end

end
