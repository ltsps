function [route, totalDistance] = ltspsGreedyHeuristic (distanceMatrix, closestNeighbour)
% ltsps-greedy
% description: a greedy algorithm that usually finds decent solutions for
% the TSP (travelling salesman problem) based on the closest (or optionally
% the farthest) neighbour heuristic.
% author: Laurens Van Houtven <lvh@laurensvh.be>
% date: 28 Sep 2008

% Constants
% Start at a random vertex?
% if true: Pick a random vertex to start from.
% if false: Start at the first vertex (ie order as given by distanceMatrix)
RANDOM_START_VERTEX = 0;

if closestNeighbour==1
    CLOSEST_NEIGHBOUR_HEURISTIC=1;
else
    CLOSEST_NEIGHBOUR_HEURISTIC=0;
end

% Initialisation
% How big is our set?
matrixDims = size(distanceMatrix);
setSize = matrixDims(2);
% The route (vector of vertex indices). Preallocating means avoiding
% in-looop growth. One city (the first one) occurs twice, hence n+1.
route = zeros(1,setSize+1);
% Unvisited cities. 1 if the city at that index is unvisited, 0 otherwise.
unvisited = ones(setSize,1);
% Route length
totalDistance = 0;
% What vertex do we start at?
if (RANDOM_START_VERTEX)
	currentVertex = unidrnd(setSize);
else
	currentVertex = 1;
end


for i = 1:(setSize)
    % Visit the city.
    unvisited(currentVertex) = 0;
    
    % Add it to the route.
    route(i) = currentVertex;
    
    % Get the source distance vector.
    % We assume that the distance matrix should be interpreted as follows:
    % rows   are where you're coming from (source)
    % colums are where you're going (destination)
    sourceDistances = distanceMatrix(currentVertex,:);
    
    % Make the distances to nodes we've already visited zero.
    % unvisited is a column vector, sourceDistances is a row, it's slightly
    % more logical to have sourceDistances have the same dims as distances.
	distances = sourceDistances .* unvisited';
        
    % Find index of the the closest city.
    if CLOSEST_NEIGHBOUR_HEURISTIC
        % Make the distances to each node we've already visited *really*
        % big. Motivation: interpret "distance" as "cost to get there", so
        % going back to a node where one has been already should be almost
        % infinitely expensive. In this case, the biggest real number
        % MATLAB knows about.
        
	    distances(distances==0)=realmax;
        [distance, distanceIndex] = min(distances); % closest
    else % FARTHEST_NEIGHBOUR_HEURISTIC
        [distance, distanceIndex] = max(distances); % farthest
    end
        
    % Travel the distance to the next city.
    if distance ~= realmax
        totalDistance = totalDistance + distance;
    end
        
	% Set the currentVertex to point to the next city.
	currentVertex = distanceIndex;
end

% Make the route cyclic:
route(end) = route(1);

end